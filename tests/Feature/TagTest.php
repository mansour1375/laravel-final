<?php

namespace Tests\Feature;

use App\Tag;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class TagTest extends TestCase
{
    use RefreshDatabase;
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create(['role_id'=>2]);
    }

    public function test_tag_lists_can_be_retrieved()
    {
        $tag = factory(Tag::class)->create();
        $tag1 = factory(Tag::class)->create(['name'=>'manahita','dress_id'=>1]);
        $tag2= factory(Tag::class)->create(['name'=>'mogoli','dress_id'=>2]);

        $response = $this->get('api/Tags/m');
        $response->assertJson([
           ['name'=>'mansour']
        ]);

        $response->assertStatus(Response::HTTP_FOUND);
    }


    public function test_tag_lists_can_be_added()
    {
        $response = $this->post('api/Tags',$this->data());
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson([
            'name'=>'tagname',
        ]);
    }

    public function test_tag_can_be_deleted()
    {
        $tag =factory(Tag::class)->create();
        $response =$this->delete('api/Tags/'.$tag->id.'?api_token='.$this->user->api_token);
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertCount(0,Tag::all());


    }

    public function data(){
        return[
          'name'=>'tagname',
          'dress_id'=>1,
          'api_token'=>$this->user->api_token
        ];
    }

}
