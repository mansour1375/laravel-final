<?php

namespace Tests\Feature;

use App\Store;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PhpParser\Node\Stmt\DeclareDeclare;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class StoreTest extends TestCase
{
    use RefreshDatabase;
    protected $user;



    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_a_authenticated_user_can_add_store()
    {
        $response = $this->post('/api/Stores',$this->data());

        $response->assertStatus(Response::HTTP_CREATED);

        $store = Store::first();
        $this->user->refresh();
        $this->assertEquals($this->user->role_id ,2);
        $this->assertEquals('mansourStore',$store->name);
        $this->assertEquals('someUrl',$store->logo);
        $this->assertEquals('my store description',$store->description);
        $this->assertEquals('my store bio',$store->bio);
        $this->assertEquals('my store address',$store->address);
    }


    public function test_an_authenticated_user_can_edit_store()
    {
        $store = factory(Store::class)->create(['user_id'=> $this->user->id]);

        $response = $this->patch('/api/Stores/'.$store->id , $this->data());

        $response->assertStatus(Response::HTTP_OK);

        $store = Store::first();
        $this->assertEquals('mansourStore',$store->name);
        $this->assertEquals('someUrl',$store->logo);
        $this->assertEquals('my store description',$store->description);
        $this->assertEquals('my store bio',$store->bio);
        $this->assertEquals('my store address',$store->address);
    }

    public function test_an_authenticated_user_only_can_edit_its_store()
    {
        $store = factory(Store::class)->create(['user_id'=> $this->user->id]);
        $otherUser = factory(User::class)->create();

        $response = $this->patch('/api/Stores/'.$store->id , array_merge($this->data(),['api_token'=>$otherUser->api_token]));
        $response->assertStatus(Response::HTTP_FORBIDDEN);

    }

    public function test_an_unauthenticated_user_can_retrieve_store()
    {
        //create it
        $store = factory(Store::class)->create(['user_id'=> $this->user->id]);
        //retrieve it
        $response = $this->get('/api/Stores/' . $store->id .'?api_token=' . $this->user->api_token);
        //check if its here
        $response->assertStatus(Response::HTTP_FOUND);
        /*dd(json_decode($response->content()));*/
       $response->assertJson([
            [
                'id'=>$store->id
            ]
        ]);
        /*dd($response->getContent());*/
    }

    public function test_an_authenticated_user_can_soft_delete_store()
    {
        //create it
        $store = factory(Store::class)->create(['user_id'=> $this->user->id]);
        $this->assertCount(1,Store::all());
        //delete it
        $response = $this->delete('/api/Stores/'. $store->id.'?api_token='.$this->user->api_token );
        //check if its deleted
        $this->assertCount(0,Store::withoutTrashed()->get());
        $this->assertCount(1,Store::onlyTrashed()->get());
        $response->assertStatus(Response::HTTP_NO_CONTENT);


    }

    public function test_an_authenticated_admin_can_Force_delete_store()
    {
        $user = factory(User::class)->create(['role_id' => 1]);
        //create it
        $store = factory(Store::class)->create(['user_id'=> $this->user->id]);
        $this->assertCount(1,Store::all());
        //delete it
        $response = $this->delete('/api/Stores/admin/'. $store->id .'?api_token='.$user->api_token );
        //check if its deleted
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertCount(0,Store::all());


    }

    public function test_an_authenticated_admin_can_restore_an_store()
    {
        $this->withExceptionHandling();
        //create it
        $user = factory(User::class)->create(['role_id' => 1]);
        //dd($user);
        $store = factory(Store::class)->create(['user_id' => $this->user->id]);

        $store->delete();

        $store->refresh();


        $this->assertEquals(1,Store::onlyTrashed()->get()->count());
        //restore
        $response = $this->get('/api/Stores/admin/restore/'. $store->id .'?api_token='. $user->api_token );
        //$this->assertCount(1,Store::all());

        $store->refresh();

        //check if its restored
        //$this->assertEquals(1,Store::withoutTrashed()->get()->count());
        $response->assertStatus(Response::HTTP_OK);

    }





    public function data(){
        return [
            'name'=>'mansourStore',
            'logo'=>'someUrl',
            'description'=>'my store description',
            'bio'=>'my store bio',
            'address'=>'my store address',
            'api_token'=>$this->user->api_token,

        ];
    }
}
