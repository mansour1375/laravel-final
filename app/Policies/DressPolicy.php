<?php

namespace App\Policies;

use App\Dress;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DressPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Dress  $dress
     * @return mixed
     */
    //trashed
    public function trashed(User $user, Dress $dress)
    {
        return $user->role_id == 2  and $dress->store()->user_id == $user->id or  $user->role_id == 1 ;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id == 2;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Dress  $dress
     * @return mixed
     */
    public function update(User $user, Dress $dress)
    {
        $userid = (int)$dress->store->user_id;
        return $user->role_id == 2 and  $userid == $user->id ;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Dress  $dress
     * @return mixed
     */
    public function delete(User $user, Dress $dress)
    {
        $userid = (int)$dress->store->user_id;
        return $user->role_id == 2 and  $userid == $user->id ;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Dress  $dress
     * @return mixed
     */
    public function restore(User $user, Dress $dress)
    {
        $userid = (int)$dress->store->user_id;
        return $user->role_id == 2 and  $userid == $user->id ;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Dress  $dress
     * @return mixed
     */
    public function forceDelete(User $user, Dress $dress)
    {
        $userid = (int)$dress->store->user_id;
        return ($user->role_id == 2 and  $userid == $user->id) or $user->role_id == 1 ;
    }
}
