<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    protected $guarded=[];
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function Stores()
    {
        return $this->belongsToMany('App\Store','ClientStore','client_id','store_id');
    }
    public function dresses()
    {
        return $this->belongsToMany('App\Dress','ClientDress','client_id','dress_id');
    }
    public function comments() {

        return $this->hasMany('App\Comment','client_id');
    }
}
