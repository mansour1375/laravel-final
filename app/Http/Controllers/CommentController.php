<?php

namespace App\Http\Controllers;

use App\Client;
use App\Comment;
use App\Dress;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(Dress $dress)
    {
        $this->authorize('viewAny' , Comment::class);
        return response($dress->comments()->get(),\Symfony\Component\HttpFoundation\Response::HTTP_OK);
    }


    public function show(Comment $comment)
    {
        //slug for a comment for admin to go to the comment and see it
    }


    public function store()
    {

        $this->authorize('create' , Comment::class);
        $client = Client::withoutTrashed()->where('user_id',request()->user()->id)->first();
        $comment = $client->comments()->create($this->validateData())->save();
        return \response($comment,\Symfony\Component\HttpFoundation\Response::HTTP_CREATED);
    }


    public function destroy(Comment $comment)
    {
        $this->authorize('delete' , Comment::class);

        $comment->delete();
        return \response([],\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);
    }



    private function validateData()
    {
        return  request()->validate([
            'dress_id'=>'required',
            'text'=>'required',
            'parent_id'=>''
        ]);
    }
}
