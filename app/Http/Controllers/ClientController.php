<?php

namespace App\Http\Controllers;

use App\Dress;
use App\Store;
use App\User;
use http\Client;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientController extends Controller
{
    public function index()
    {
        //policy
        return response(\App\Client::withoutTrashed()->get()->toArray(),Response::HTTP_OK);

    }
    public function trashed()
    {
        //policy
        return response(\App\Client::onlyTrashed()->get()->toArray(),Response::HTTP_OK);

    }
    public function all()
    {
        //policy
        return response(\App\Client::withTrashed()->get()->toArray(),Response::HTTP_OK);

    }


    //return the details of the user for dashboard
    public function show(\App\Client $client)
    {

        //policy admin and store
        return response($client,Response::HTTP_FOUND) ;
    }
    public function showAdmin(int $id)
    {
        //policy admin and store
        return response(\App\Client::withTrashed()->find($id),Response::HTTP_FOUND) ;
    }


    public function store()
    {


        $client = new \App\Client($this->validateDate());
        $userId = \request()->user();
        if (isset($userId) && $userId->role_id == 4 ) {

           $user =  User::query()->findOrFail((int)$userId->id);
            $client->user_id = $userId->id;
            $user->role_id = 3;
            $user->save();
            $client->save();

            return \response($client,Response::HTTP_CREATED);
        }
        return \response([],Response::HTTP_NO_CONTENT);

    }

    public function update(\App\Client $client)
    {
        //policy owner and admin
        $client->update($this->validateDate());
        \response($client,Response::HTTP_OK);
    }

    public function delete(\App\Client $client)
    {
        $client->user()->update(['role_id'=>4]);
        $client->delete();

         return response([],Response::HTTP_NO_CONTENT);
    }
    public function destroy(int $id) {


         $client = \App\Client::withTrashed()->findOrFail($id);
         User::query()->findOrFail($client->user_id)->update(['role_id'=>4]);
         $client->forceDelete();
         return \response([],Response::HTTP_NO_CONTENT);

    }

    public function restore(int $id) {


         $client = \App\Client::withTrashed()->findOrFail($id);
         User::query()->findOrFail($client->user_id)->update(['role_id'=>3]);
         $client->restore();
         return \response($client,Response::HTTP_OK);

    }

    //list of favorite shops
    public function Stores(\App\Client $client)
    {
        //policy
        return \response($client->Stores()->get(),Response::HTTP_FOUND);

    }

    //follow the store
    public function follow(Store $store,\App\Client $client)
    {
        $client->Stores()->save($store);

        return \response([],Response::HTTP_OK);


    }


    // unfollow store
    public function unfollow(Store $store,\App\Client $client)
    {

        $client->Stores()->delete($store);
        return \response($store,Response::HTTP_OK);

    }


    //list of favorite dresses
    public function likes(\App\Client $client)
    {


        return \response($client->dresses()->get(),Response::HTTP_FOUND);

    }

    //likes a dress
    public function like(Dress $dress,\App\Client $client)
    {

        $client->dresses()->save($dress);
        return \response([],Response::HTTP_OK);

    }


    //dislike a dresses
    public function dislike(Dress $dress,\App\Client $client)
    {


        $client->dresses()->delete($dress);
        return \response([],Response::HTTP_OK);
    }

    //show cart
    public function cart()
    {


    }

    //delete from cart
    public function deletecart()
    {


    }


    //add to cart
    public function addtocart()
    {
        //pivot

    }

    public function validateDate(){

        return \request()->validate([
           'name'=>'required',
           'pic'=>'required',
           'address'=>'required',
           'valet'=>'required',
           'phone'=>'required',

        ]);
    }




}
