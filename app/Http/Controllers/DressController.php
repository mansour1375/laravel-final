<?php

namespace App\Http\Controllers;

use App\Dress;
use App\Store;
use App\Tag;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;

class DressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */

    //all the dresses
    public function index(Store $store)
    {

        if($store->dresses()->get()->count()>0)
            return response(Dress::withoutTrashed()->where('store_id',$store->id)->get()->toArray(),\Symfony\Component\HttpFoundation\Response::HTTP_FOUND);
        return \response([],\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

    }

    public function all(Store $store)
    {
        $this->authorize('trashed',Dress::class);
        if($store->dresses()->get()->count()>0)
            return response(Dress::withTrashed()->where('store_id',$store->id)->get()->toArray(),\Symfony\Component\HttpFoundation\Response::HTTP_FOUND);
        return \response([],\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);

    }

    public function trashed(Store $store)
    {
        $this->authorize('trashed',Dress::class);
        return response(Dress::onlyTrashed()->where('store_id',$store->id)->get()->toArray(),\Symfony\Component\HttpFoundation\Response::HTTP_FOUND);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->authorize('create',Dress::class);
        $storeId = User::findOrFail(\request()->user()->id)->Store->id;
        $dress = new  Dress($this->validateData());
        $dress->store_id=$storeId;
        $dress->save();

        /*$dress = request()->user()->Store()->dresses()->create($this->validateData());*/

       return \response($dress,\Symfony\Component\HttpFoundation\Response::HTTP_CREATED);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dress  $dress
     * @return \Illuminate\Http\Response
     */
    public function show(Dress $dress)
    {

        return \response(Dress::withoutTrashed()->where('status','=','4s' )->find($dress),\Symfony\Component\HttpFoundation\Response::HTTP_FOUND);

    }

    public function showAdmin(Dress $dress)
    {

        return \response(Dress::withTrashed()->find($dress),\Symfony\Component\HttpFoundation\Response::HTTP_FOUND);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dress  $dress
     * @return \Illuminate\Http\Response
     */
    public function update(Dress $dress)
    {
        $this->authorize('update',$dress);

        $dress->update($this->validateData());

        return \response($dress,\Symfony\Component\HttpFoundation\Response::HTTP_OK);

    }



    /**
     * Remove the specified resource from storage.and move it to trash
     *
     * @param  \App\Dress  $dress
     * @return \Illuminate\Http\Response
     */
    public function delete(Dress $dress)
    {
        $this->authorize('delete',$dress);
         $dress->delete();

         return \response([],\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);
    }


    public function destroy(int $id)
    {
        $this->authorize('forceDelete',Dress::class);
        $dress = Dress::withTrashed()->findOrFail($id);
        $dress->forceDelete();
        return \response([],\Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);
    }


    public function restore(int $id)
    {
        $dress = Dress::onlyTrashed()->findOrFail($id);
        $this->authorize('restore',$dress);

        $dress->restore();
        return \response($dress,\Symfony\Component\HttpFoundation\Response::HTTP_OK);
    }

    public function validateData(){
        return \request()->validate([


            'size_id'=>'required',
            'category_id'=>'required',
            'pic' =>'required',
            'status'=>'required',
            'name'=>'required',
            'color'=>'required',
            'fee'=>'required',
            'description'=>'required',
            'off' => 'required',

        ]);
    }



}
