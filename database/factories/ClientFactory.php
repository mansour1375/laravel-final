<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'user_id'=> factory(\App\User::class),
        'name'=>$faker->name,
        'pic'=>$faker->url,
        'address'=>$faker->address,
        'valet'=>2000000,
        'phone'=>$faker->phoneNumber,
    ];
});
