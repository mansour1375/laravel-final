<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Store;
use App\User;
use Faker\Generator as Faker;

$factory->define(Store::class, function (Faker $faker) {
    return [
            'user_id'=> factory(User::class),
            'name'=>$faker->name,
            'logo'=>$faker->url,
            'description'=>$faker->text(25),
            'bio' => $faker->text(20),
            'address'=>$faker->address,
    ];
});
