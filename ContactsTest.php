<?php

namespace Tests\Feature;

use App\Contact;
use App\User;
use http\Env\Response;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response as Response_Status;
use Tests\TestCase;

class ContactsTest extends TestCase
{
    use RefreshDatabase;

    // test_an_unauthenticated_user_should_redirected_to_login
    // an_authenticated_user_can_add_a_contact
    // fields_are_required
    // invalid_Email_did_not_added
    // birthdays_are_properly_stored
    // test_a_contact_can_be_retrieved
    // test_only_the_users_contacts_can_be_retrieved
    // test_a_contact_can_be_patched
    // test_only_the_owner_the_contact_can_patch_the_contact
    // test_a_contact_can_be_deleted
    // test_a_list_of_contact_can_be_fetched_for_authenticated_user
    protected $user;
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function test_an_unauthenticated_user_should_redirected_to_login()
    {
        $response =  $this->post('/api/contacts', array_merge($this->data(),['api_token'=>'']));

        $response->assertRedirect('/login');

        $this->assertCount(0,Contact::all());

    }

    /** @test  */
    public function an_authenticated_user_can_add_a_contact()
    {
        $this->withoutExceptionHandling();



         $response = $this->post('/api/contacts', $this->data());

        /*dd(json_decode($response->getContent()));*/
        $contact = Contact::first();
        $this->assertEquals('Test Name',$contact->name);
        $this->assertEquals('05/04/1998',$contact->birthday->format('m/d/Y'));
        $this->assertEquals('ABC company',$contact->company);
        $this->assertEquals('m137ah@gmail.com',$contact->email);
        $response->assertStatus(Response_Status::HTTP_CREATED);

        $response->assertJson([
            'data'=>[
                'contact_id'=> $contact->id,
            ],
            'links'=>[
                'self' => $contact->path(),
            ]
        ]);


    }


    /** @test  */
    public function fields_are_required()
    {
        collect(['name','email','company','birthday'])
        ->each(function ($field){
            $response = $this->post('/api/contacts', array_merge($this->data() , [$field=>'']));

            $contact = Contact::first();

            $response->assertSessionHasErrors($field);
            $this->assertCount(0,Contact::all());
        });
    }


    /** @test  */
    public function invalid_Email_did_not_added()
    {
        $response = $this->post('/api/contacts', array_merge($this->data() , ['email'=>'NOT AN EMAIL']));

        $contact = Contact::first();
        $response->assertSessionHasErrors('email');
        $this->assertCount(0,Contact::all());
    }


    /** @test  */
    public function birthdays_are_properly_stored()
    {
        $response = $this->post('/api/contacts', array_merge($this->data()));

        $contact = Contact::first();


        $this->assertCount(1,Contact::all());
        $this->assertInstanceOf(Carbon::class,Contact::first()->birthday);
        $this->assertEquals('05-04-1998' , Contact::first()->birthday->format('m-d-Y'));
    }


    public function test_a_contact_can_be_retrieved()
    {
        $contact = factory(Contact::class)->create(['user_id' => $this->user->id]);
        $response = $this->get('api/contacts/'.$contact->id.'?api_token='.$this->user->api_token);

        $response->assertJson([
               'data'=> [
                   'contact_id'=>$contact->id,
                   'name' => $contact->name,
                   'email'=> $contact->email,
                   'birthday' => $contact->birthday->format('d/m/Y'),
                   'company'=> $contact->company,
                   'last_update'=>$contact->updated_at->diffForHumans(),
                ]
            ]);

    }


    public function test_only_the_users_contacts_can_be_retrieved()
    {
        $contact = factory(Contact::class)->create(['user_id' => $this->user->id]);

        $anotheruser = factory(User::class)->create();

        $response = $this->get('api/contacts/'.$contact->id.'?api_token='.$anotheruser->api_token);

        $response->assertStatus(403);
    }


    public function test_a_contact_can_be_patched()
    {
        $this->withoutExceptionHandling();

        $contact = factory(Contact::class)->create(['user_id'=>$this->user->id]);



        $response = $this->patch('api/contacts/'.$contact->id , $this->data());

        $contact->refresh();

        $this->assertEquals('Test Name',$contact->name);
        $this->assertEquals('05/04/1998',$contact->birthday->format('m/d/Y'));
        $this->assertEquals('ABC company',$contact->company);
        $this->assertEquals('m137ah@gmail.com',$contact->email);
        $response->assertStatus(Response_Status::HTTP_OK);
        $response->assertJson([
            'data'=>[
                'contact_id'=>$contact->id,
            ],
            'links'=>[
                'self'=> $contact->path(),
            ]
        ]);
    }


    public function test_only_the_owner_the_contact_can_patch_the_contact()
    {
        $contact = factory(Contact::class)->create();
        $anotherUser = factory(User::class)->create();
        $response = $this->patch('api/contacts/'.$contact->id,array_merge($this->data() , ['api_token'=>$anotherUser->api_token]));
        $response->assertStatus(403);

    }


    public function test_a_contact_can_be_deleted()
    {
        $contact = factory(Contact::class)->create(['user_id' => $this->user->id]);

        $response = $this->delete('api/contacts/'.$contact->id ,['api_token'=>$this->user->api_token]);

        $this->assertCount(0,Contact::all());

        $response->assertStatus(Response_Status::HTTP_NO_CONTENT);
    }



    public function test_only_the_owner_can_deleted_the_contact()
    {
        $contact = factory(Contact::class)->create();

        $anotherUser = factory(User::class)->create();
        $response = $this->delete('api/contacts/'.$contact->id ,['api_token'=>$anotherUser->api_token]);

        $response->assertStatus(403);
    }



    public function test_a_list_of_contact_can_be_fetched_for_authenticated_user()
    {

        $user = factory(User::class)->create();
        $otheruser = factory(User::class)->create();

        $contact =factory(Contact::class)->create(['user_id'=>$user->id]);
        $Othercontact =factory(Contact::class)->create(['user_id'=>$otheruser->id]);

        $response =$this->get('/api/contacts?api_token='.$user->api_token);

        $response->assertJsonCount(1)->assertJson(
        [
            'data'=>[
                [
                   'data'=>
                       [
                           'contact_id'=>$contact->id
                       ],
                ]
            ]
        ]);

    }


    private function data()
    {
        return [
            'name' =>'Test Name',
            'email'=>'m137ah@gmail.com',
            'birthday'=>'05/04/1998',
            'company'=>'ABC company',
            'api_token'=> $this->user->api_token,
        ];
    }

}
