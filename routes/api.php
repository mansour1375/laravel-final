<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|API Routes
*/

Route::middleware('auth:api')->group(function (){

    Route::get('/contacts','ContactsController@index');
    Route::get('/contacts/{contact}','ContactsController@show');
    Route::post('/contacts','ContactsController@store');
    Route::patch('/contacts/{contact}','ContactsController@update');
    Route::delete('/contacts/{contact}','ContactsController@destroy');

    //must be refactored
    //store-------------------------------------------------------
    //seller
    Route::get('/Stores','StoreController@index');
    Route::get('/Stores/{store}','StoreController@show');
    Route::post('/Stores','StoreController@store');
    Route::patch('/Stores/{store}','StoreController@update');
    Route::delete('/Stores/{store}','StoreController@delete');
    //admin
    Route::get('/Stores/admin/trashed','StoreController@trashed');
    Route::get('/Stores/admin/restore/{id}','StoreController@restore');
    Route::delete('/Stores/admin/{id}','StoreController@destroy');

    //dresses------------------------------------------------------
    //Admin
    Route::get('/Dresses/{dress}','DressController@show')->withoutMiddleware('auth:api');
    Route::get('/Dresses/Store/{store}','DressController@index')->withoutMiddleware('auth:api');
    Route::get('/Dresses/Store/trashed','DressController@trashed');
    Route::get('/Dresses/Store/restore/{id}','DressController@restore');
    Route::get('/Dresses/Store/admin/{dress}','DressController@showAdmin');
    Route::post('/Dresses','DressController@store');
    Route::patch('/Dresses/{dress}','DressController@update');
    Route::delete('/Dresses/{dress}','DressController@delete');
    Route::delete('/Dresses/destroy/{id}','DressController@destroy');

    //client -------------------------------------------------------
    Route::get('/Client/Admin/trashed','ClientController@trashed');
    Route::get('/Client/Admin/restore/{id}','ClientController@restore');
    Route::get('/Client/Admin/{id}','ClientController@showAdmin');
    Route::get('/Client/{client}','ClientController@show');
    //following shops
    Route::get('/Client/Stores/{client}','ClientController@Stores');//show
    Route::get('/Client/follow/{store}/{client}','ClientController@follow');//follow
    Route::delete('/Client/follow/{store}/{client}','ClientController@unfollow');//unfollow
    //dresses that client liked so far
    Route::get('/Client/likes/{client}','ClientController@likes');
    Route::get('/Client/like/{dress}/{client}','ClientController@like');
    Route::delete('/Client/like/{dress}/{client}','ClientController@dislike');

    Route::post('/Client','ClientController@store');
    Route::patch('/Client/{client}','ClientController@update');
    Route::delete('/Client/Admin/{client}','ClientController@delete');
    Route::delete('/Client/Admin/destroy/{id}','ClientController@destroy');

    //Comments-------------------------------------------------------

    Route::post('/comment','CommentController@store');
    Route::get('/comment/{dress}','CommentController@index');
    Route::delete('/comment/{comment}','CommentController@destroy');

    //Category------------------------------------------------------

    Route::get('/category','CategoryController@index')->withoutMiddleware('auth:api');
    Route::post('/category','CategoryController@store');
    Route::patch('/category/{category}','CategoryController@update');
    Route::delete('/category/{category}','CategoryController@destroy');

    //Tags-----------------------------------------------------------
    Route::get('/Tags/{name}','TagController@index')->withoutMiddleware('auth:api');
    Route::get('/Tags/{dress}','TagController@show')->withoutMiddleware('auth:api');
    Route::post('/Tags','TagController@store');
    Route::delete('/Tags/{tag}','TagController@destroy');

    //Size ----------------------------------------------------------
    Route::post('/Size','SizeController@store');
    Route::delete('/Size/{size}','SizeController@destroy');
    //search -------------------------------------------------------
    Route::get('/Search/tag/{tag}','SearchController@tag')->withoutMiddleware('auth:api');
    Route::get('/Search/category/{category}','SearchController@category')->withoutMiddleware('auth:api');
    Route::get('/Search/size/{size}','SearchController@Size')->withoutMiddleware('auth:api');
    Route::get('/Search/fee/{max}/{min}','SearchController@fee')->withoutMiddleware('auth:api');

});





